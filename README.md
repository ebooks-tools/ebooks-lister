# eBooks Lister


[![pypi](https://img.shields.io/pypi/v/ebooks-lister.svg)](https://pypi.org/project/ebooks-lister/)
[![python](https://img.shields.io/pypi/pyversions/ebooks-lister.svg)](https://pypi.org/project/ebooks-lister/)
[![Build Status](https://gitlab.com/ebooks-tools/ebooks-lister/badges/main/pipeline.svg)](https://gitlab.com/ebooks-tools/ebooks-lister/-/pipelines/)
[![codecov](https://codecov.io/gh/mattkatz/ebooks-lister/branch/main/graphs/badge.svg)](https://codecov.io/github/mattkatz/ebooks-lister)



Get normalized metadata from paths with ebooks


* Documentation: <https://ebooks-tools.gitlab.io/ebooks-lister/>
* Code Repository: <https://gitlab.com/ebooks-tools/ebooks-lister>
* PyPI: <https://pypi.org/project/ebooks-lister/>
* Free software: MIT


## Features

* TODO

## Credits

This package was created with [Cookiecutter](https://github.com/audreyr/cookiecutter) and the [waynerv/cookiecutter-pypackage](https://github.com/waynerv/cookiecutter-pypackage) project template.
