# Usage

## In a project

To use eBooks Lister in a project

```py
from pathlib import Path
import ebooks_lister
# define a Path (or string of a Path) to your ebooks
paths = [Path('~/Documents/mybooks'),]
# define the fields to extract
fields = ['author','title','isbn']
lister = EbookLister(paths, fields)
for listing in lister:
    print(f"{path=} has an ebook by {author=} with {title=} and the {isbn=}.})
```

## From a commandline
You call the command line interface by passing in path arguments that are directories that contain ebooks, fields arguments that are the fields you want to extract from the metadata. This will ouput a listing to standard out.

Optionally, you can pass a path to an outfile - it will be overwritten with a csv listing of the metadata.

The first field in either listing is always the path to the ebook file the metadata came from.
```zsh
$ ebooks-lister --path ~/Documents/book_library_directory1 --path /shared/our/group/books/directory/ --path /another/book/directory --fields creator title isbn --outfile ~/Documents/my_books.csv
```
Here is the --help output from the commmand line script
```zsh
$ ebooks-lister --help
usage: ebooks-lister [-h] [--path PATHS [PATHS ...]] [--fields FIELDS [FIELDS ...]] [--outfile OUTFILE]

Get normalized metadata from paths with ebooks

options:
  -h, --help            show this help message and exit
  --path PATHS [PATHS ...], --paths PATHS [PATHS ...]
                        path or paths to directories of ebooks
  --fields FIELDS [FIELDS ...]
                        a list of fields to extract from ebook metadata
  --outfile OUTFILE     path to write a csv file to.

```
