sources = ebooks_lister

.PHONY: test format lint unittest coverage pre-commit clean
test: unittest format lint 

format:
	isort $(sources) tests
	black $(sources) tests

lint:
	flake8 $(sources) tests
	mypy $(sources) tests

unittest:
	pytest

coverage:
	pytest --cov=$(sources) --cov-branch --cov-report=term-missing tests

pre-commit:
	pre-commit run --all-files

clean:
	rm -rf .mypy_cache .pytest_cache
	rm -rf *.egg-info
	rm -rf .tox dist site
	rm -rf coverage.xml .coverage

watch-docs:
	mkdocs serve

watch:
	watchmedo shell-command --command "clear;make test" --pattern "*.py" --recursive --drop

devenv:
	poetry install -E doc -E dev -E test
